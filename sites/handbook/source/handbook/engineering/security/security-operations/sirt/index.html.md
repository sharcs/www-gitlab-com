---
layout: handbook-page-toc
title: Security Incident Response Team - SIRT
description: GitLab Security Incident Response Team Overview 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

The Security Incident Response Team - SIRT is on the forefront of security events that impact both GitLab.com and GitLab the company.

## Our Vision

To detect security incidents before they happen and to respond promptly when they do happen. 

## Our Mission

### Incident tracking

Incidents are tracked in the [Operations tracker](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/) through the use of the [incident template](https://gitlab.com/gitlab-com/gl-security/security-operations/sirt/operations/-/blob/master/.gitlab/issue_templates/incident_response.md).

The correct use of dedicated **scoped incident labels** is critical to the sanity of the data in the incident tracker and the subsequent metrics gathering from it.

#### Incident labels description

* **Incident delineator** `incident` - denotes that an issue should be considered an incident and tracked as such.
* `Incident::Phase` - what stage is the incident at?
    * `Incident::Phase::Identification` - Incident is currently being triaged (log dives, analysis, and verification)
    * `Incident::Phase::Containment` - Limiting the damage (mitigations being put in place)
    * `Incident::Phase::Eradication` - Cleaning, restoring, removing affected systems, or otherwise remediating findings
    * `Incident::Phase::Recovery` - Testing fixes, restoring services, transitioning back to normal operations
    * `Incident::Phase::IncidentReview` - The incident review process has begun (required for all S1/P1 incidents)
    * `Incident::Phase::Closed` - Incident is completely resolved
* `Incident::Category` - what is the nature of the incident?
    * `Incident::Category::Abuse` - Abusive activity impacted GitLab.com
    * `Incident::Category::CustomerRequest`
    * `Incident::Category::DataLoss` - Loss of data
    * `Incident::Category::InformationDisclosure` - Confidential information might have been disclosed to untrusted parties
    * `Incident::Category::LostStolenDevice` - Laptop or mobile device was lost or stolen
    * `Incident::Category::Malware` - Malware
    * `Incident::Category::Misconfiguration` - A service misconfiguration
    * `Incident::Category::NetworkAttack` - Incident due to malicious network activity - DDoS, cred stuffing
    * `Incident::Category::NotApplicable` - Used to denote a false positive incident (such as an accidental page)
    * `Incident::Category::Phishing` - Phishing
    * `Incident::Category::UnauthorizedAccess` - Data or systems were accessed without authorization
    * `Incident::Category::Vulnerability` - A vulnerability in GitLab and/or a service used by the organization has lead to a security incident
* `Incident::Organization` - what is impacted?
    * `Incident::Organization::AWS` - One of GitLab's AWS environments
    * `Incident::Organization::Azure` - GitLab's Azure environment
    * `Incident::Organization::GCP` - GitLab's GCP environment
    * `Incident::Organization::GCPEnclave` - GitLab Security's GCP environment
    * `Incident::Organization::GSuite` - Google Workspaces (GSuite, GDrive)
    * `Incident::Organization::DO` - Digital Ocean environment
    * `Incident::Organization::GitLab` - GitLab the organization and GitLab the product
    * `Incident::Organization::SaaS` - Incident in vendor-operated SaaS platform
    * `Incident::Organization::GitLabPages` - GitLab.com Pages
    * `Incident::Organization::EnterpriseApps` - Other enterprise apps not defined here (Zoom, Slack, etc)
* `Incident::Source` - how did SIRT learn of the incident?
* `Incident::Origin` - how did GitLab learn of the incident?
* `Incident::Classification` - how accurate was the finding?
    * `Incident::Classification::TruePositive`
    * `Incident::Classification::FalsePositive`
    * `Incident::Classification::TrueNegative`
    * `Incident::Classification::FalseNegative`

